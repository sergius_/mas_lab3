import time
from collections import deque
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

from agents.random_agent import RandomAgent


def train(env, agents, episodes):
    for index in agents.keys():
        agents[index].prioritized_memory.beta_annealing_steps = 50 * episodes

    start = time.time()

    scores_current = {
        "agent0": deque(maxlen=100),
        "agent1": deque(maxlen=100),
        "adversary": deque(maxlen=100),
    }

    score_history = {
        "agent0": [],
        "agent1": [],
        "adversary": [],
    }

    means = {
        "agent0": [],
        "agent1": [],
        "adversary": [],
    }

    for episode in tqdm(range(episodes)):
        env.reset()
        cycles = 0
        episode_score = {
            "agent0": 0,
            "agent1": 0,
            "adversary": 0,
        }

        for agent_env_name in env.agent_iter():
            agent_name = translate_agent_name(agent_env_name)
            agent = agents.get(agent_name)

            if episode % 100 == 0:
                print(agent_name, " epsilon: ", agent.get_epsilon())

            if (episode == 1000) & (agent_name != "adversary"):
                agent.reset_epsilon()

            observation, reward, done, trunc, info = env.last()
            # Tag observations with the agent index
            if agent.agent_index is not None:
                observation = np.append(observation, [agent.agent_index])

            action = agent.make_action(observation, done, trunc)
            env.step(action)

            if agent.last_observation is not None and agent.last_action is not None:
                agent.push_memory(agent.last_observation, agent.last_action, reward, observation, done)
                agent.prioritized_memory.push(agent.last_observation, agent.last_action, reward, observation, done)

            prioritized = True
            if prioritized:
                agent.do_prioritized_training_update((episode * 50) + cycles)
            else:
                agent.do_training_update()

            agent.last_observation = observation
            agent.last_action = action

            if cycles % 5 == 0:
                agent.update_target_network()

            agent.decay_epsilon()
            episode_score[agent_name] += reward
            cycles += 1

        for agent in episode_score.keys():
            scores_current[agent].append(episode_score[agent])
            score_history[agent].append(episode_score[agent])
            mean = sum(scores_current[agent]) / len(scores_current[agent])
            means[agent].append(mean)

        if episode % 100 == 0:
            make_plot(means)
            print(f"Episode {episode} complete!")
            for agent in agents.keys():
                agents[agent].save_model(f"{agent}_model")
            print("Mean scores last 100 episodes:")
            for agent in means.keys():
                print(f"{agent}: {means[agent][-1]}")
            print("\n")

    env.close()
    print(f'Time elapsed: {time.time() - start} seconds')


def test(env, agents, episodes):
    for agent_name in agents.keys():
        if not isinstance(agents[agent_name], RandomAgent):
            agents[agent_name].load_model(f"{agent_name}_model")

    # Disable random exploration
    for agent_name in agents.keys():
        agents[agent_name].epsilon = 0

    start = time.time()

    scores_current = {
        "agent0": deque(maxlen=100),
        "agent1": deque(maxlen=100),
        "adversary": deque(maxlen=100),
    }

    score_history = {
        "agent0": [],
        "agent1": [],
        "adversary": [],
    }

    means = {
        "agent0": [],
        "agent1": [],
        "adversary": [],
    }

    for episode in range(episodes):
        env.reset()
        episode_score = {
            "agent0": 0,
            "agent1": 0,
            "adversary": 0,
        }

        for agent_env_name in env.agent_iter():
            agent_name = translate_agent_name(agent_env_name)
            agent = agents.get(agent_name)
            observation, reward, done, trunc, info = env.last()
            # Tag observations with the agent index
            if agent.agent_index is not None:
                observation = np.append(observation, [agent.agent_index])

            action = agent.make_action(observation, done, trunc)
            env.step(action)

            time.sleep(0.01)

            episode_score[agent_name] += reward

        for agent in episode_score.keys():
            scores_current[agent].append(episode_score[agent])
            score_history[agent].append(episode_score[agent])
            mean = sum(scores_current[agent]) / len(scores_current[agent])
            means[agent].append(mean)

        if episode % 100 == 0:
            print(f"Episode {episode} complete!")
            print("Mean scores last 100 episodes:")
            for agent in means.keys():
                print(f"{agent}: {means[agent][-1]}")
            print("\n")

    env.close()
    make_plot(means)
    print(f'Time elapsed: {time.time() - start} seconds')
    print(f"Overall mean scores:")
    for agent in score_history:
        mean = sum(score_history[agent]) / len(score_history[agent])
        print(f"{agent}: {mean}")


def make_plot(means):
    plt.figure(figsize=(10, 5))
    plt.title("Score / Episodes")
    plt.plot(means["agent0"], label="Agent rolling mean")
    plt.plot(means["adversary"], label="Adversary rolling mean")
    plt.ylabel("Score")
    plt.xlabel("Episode")
    plt.legend()
    plt.show()


def translate_agent_name(env_agent_name):
    if env_agent_name == "agent_0":
        return "agent0"
    elif env_agent_name == "agent_1":
        return "agent1"
    elif env_agent_name == "adversary_0":
        return "adversary"
