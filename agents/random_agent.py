import random


class RandomAgent:
    possible_actions = [0, 4]

    def make_action(self, observation, done):
        if done:
            return None
        else:
            return random.randint(self.possible_actions[0], self.possible_actions[1])
