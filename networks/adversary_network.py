import torch


class AdversaryNetwork(torch.nn.Module):
    def __init__(self):
        super(AdversaryNetwork, self).__init__()
        self.fc1 = torch.nn.Linear(8, 32)
        self.fc2 = torch.nn.Linear(32, 64)
        self.fc3 = torch.nn.Linear(64, 5)

    def forward(self, x):
        x = torch.nn.functional.relu(self.fc1(x))
        x = torch.nn.functional.relu(self.fc2(x))
        x = self.fc3(x)
        return x
