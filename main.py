import argparse

from agents.deep_agent import DeepAgent
from networks.agent_network import AgentNetwork
from networks.adversary_network import AdversaryNetwork
from pettingzoo.mpe import simple_adversary_v2


from utils import train, test

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--train", help="Run in train mode", action="store_true")
    parser.add_argument("-t", "--test", help="Run in test mode", action="store_true")
    parser.add_argument("-r", "--render", help="Render game", action="store_true")
    args = parser.parse_args()

    agent0_network, agent1_network, adversary_network = AgentNetwork(), AgentNetwork(), AdversaryNetwork()
    agent0_network_target, agent1_network_target, adversary_network_target = AgentNetwork(), AgentNetwork(), AdversaryNetwork()

    agent0 = DeepAgent(epsilon=1.0, min_epsilon=0.01, decay_rate=0.999,
                       learning_rate=0.001, gamma=0.99, batch_size=64,
                       tau=0.001, q_network=agent0_network, target_network=agent0_network_target,
                       max_memory_length=100000)

    agent1 = DeepAgent(epsilon=1.0, min_epsilon=0.01, decay_rate=0.999,
                       learning_rate=0.001, gamma=0.99, batch_size=64,
                       tau=0.001, q_network=agent1_network, target_network=agent1_network_target,
                       max_memory_length=100000)

    adversary = DeepAgent(epsilon=1.0, min_epsilon=0.01, decay_rate=0.999,
                          learning_rate=0.001, gamma=0.99, batch_size=64,
                          tau=0.001, q_network=adversary_network, target_network=adversary_network_target,
                          max_memory_length=100000)

    agents = {
        "agent0": agent0,
        "agent1": agent1,
        "adversary": adversary
    }

    if args.train:
        env = simple_adversary_v2.env(N=2, max_cycles=50)
        train(env, agents, 1000)
    elif args.test:
        if args.render:
            env = simple_adversary_v2.env(N=2, max_cycles=50, render_mode="human")
        else:
            env = simple_adversary_v2.env(N=2, max_cycles=50)

        test(env, agents, 100)
